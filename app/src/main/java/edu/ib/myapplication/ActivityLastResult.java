package edu.ib.myapplication;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


public class ActivityLastResult extends Activity {

    LineGraphSeries<DataPoint> series1;
    LineGraphSeries<DataPoint> series2;
    LineGraphSeries<DataPoint> series3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivitySendFiles.csv != null && ActivitySendFiles.csv.split("\n").length>1) {
            setContentView(R.layout.activity_last_result);
            GraphView graphView = (GraphView) findViewById(R.id.graph);
            String title = ActivitySendFiles.title.split("/")
                    [ActivitySendFiles.title.split("/").length - 1]
                    .replace("Data", "")
                    .replace("_", " ")
                    .replace(".csv", "");
            title = title.substring(0, title.length() - 2) + ":" + title.substring(title.length() - 2);
            graphView.setTitle(title);
            GridLabelRenderer gridLabel = graphView.getGridLabelRenderer();
            gridLabel.setHorizontalAxisTitle("t [s]");
            gridLabel.setVerticalAxisTitle("a [m/s^2]");
            series1 = new LineGraphSeries<DataPoint>();
            series2 = new LineGraphSeries<DataPoint>();
            series3 = new LineGraphSeries<DataPoint>();
            String[] data = ActivitySendFiles.csv.split("\n");
            boolean first = true;
            double accxAvg = 0;
            double accyAvg = 0;
            double acczAvg = 0;
            double accxMax = 0;
            double accyMax = 0;
            double acczMax = 0;
            double totalTime = 0;
            for (String line : data) {
                if (first) {
                    first = false;
                    continue;
                }
                String[] records = line.split(",");
                accxAvg += Double.parseDouble(records[1]);
                accyAvg += Double.parseDouble(records[2]);
                acczAvg += Double.parseDouble(records[3]);
                if (Math.abs(Double.parseDouble(records[1])) > Math.abs(accxMax))
                    accxMax = Double.parseDouble(records[1]);
                if (Math.abs(Double.parseDouble(records[2])) > Math.abs(accyMax))
                    accyMax = Double.parseDouble(records[2]);
                if (Math.abs(Double.parseDouble(records[3])) > Math.abs(acczMax))
                    acczMax = Double.parseDouble(records[3]);
                series1.appendData(new DataPoint(Double.parseDouble(records[0]),
                        Double.parseDouble(records[1])), true, data.length);
                series2.appendData(new DataPoint(Double.parseDouble(records[0]),
                        Double.parseDouble(records[2])), true, data.length);
                series3.appendData(new DataPoint(Double.parseDouble(records[0]),
                        Double.parseDouble(records[3])), true, data.length);

                totalTime = Double.parseDouble(data[data.length - 1].split(",")[0])/1000;
            }
            accxAvg /= data.length;
            acczAvg /= data.length;
            accyAvg /= data.length;
            series1.setThickness(3);//Color of Line to be Drawn
            series1.setDataPointsRadius(7);
            series1.setColor(Color.RED);
            series2.setThickness(3);//Color of Line to be Drawn
            series2.setDataPointsRadius(7);
            series2.setColor(Color.BLUE);      //Color of Line to be Drawn
            series3.setDataPointsRadius(7);
            series3.setColor(Color.GREEN);      //Color of Line to be Drawn
            Canvas canvas = new Canvas();
            series2.draw(graphView, canvas, true);
            series1.setTitle("AccelerationX");
            series2.setTitle("AccelerationY");
            series3.setTitle("AccelerationZ");
            graphView.getGridLabelRenderer().setPadding(50);
            graphView.getGridLabelRenderer().setVerticalLabelsColor(Color.RED);
            graphView.getGridLabelRenderer().setHorizontalLabelsColor(Color.TRANSPARENT);
            graphView.getLegendRenderer().setVisible(true);
            graphView.addSeries(series1);

            Switch xSwitch  = findViewById(R.id.XVisible);
            Switch ySwitch  = findViewById(R.id.YVisible);
            Switch zSwitch  = findViewById(R.id.ZVisible);
            xSwitch.setChecked(true);
            xSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
                graphView.removeAllSeries();
                if (xSwitch.isChecked()){
                    graphView.addSeries(series1);
                }
                if (ySwitch.isChecked()){
                    graphView.addSeries(series2);
                }
                if (zSwitch.isChecked()){
                    graphView.addSeries(series3);
                }
            });
            ySwitch.setOnCheckedChangeListener((compoundButton, b) -> {
                graphView.removeAllSeries();
                if (xSwitch.isChecked()){
                    graphView.addSeries(series1);
                }
                if (ySwitch.isChecked()){
                    graphView.addSeries(series2);
                }
                if (zSwitch.isChecked()){
                    graphView.addSeries(series3);
                }
            });
            zSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
                graphView.removeAllSeries();
                if (xSwitch.isChecked()){
                    graphView.addSeries(series1);
                }
                if (ySwitch.isChecked()){
                    graphView.addSeries(series2);
                }
                if (zSwitch.isChecked()){
                    graphView.addSeries(series3);
                }
            });

            TextView averageAccelerationX = (TextView) findViewById(R.id.tvAverageAccelerationX);
            averageAccelerationX.setText("Average acceleration X axis: " + String.valueOf(accxAvg)
                    .substring(0, 8) + " m/s^2");
            TextView averageAccelerationY = (TextView) findViewById(R.id.tvAverageAccelerationY);
            averageAccelerationY.setText("Average acceleration Y axis: " + String.valueOf(accyAvg)
                    .substring(0, 8) + " m/s^2");
            TextView averageAccelerationZ = (TextView) findViewById(R.id.tvAverageAccelerationZ);
            averageAccelerationZ.setText("Average acceleration Z axis: " + String.valueOf(acczAvg)
                    .substring(0, 8) + " m/s^2");
            TextView maxAccelerationX = (TextView) findViewById(R.id.tvMaxAccX);
            maxAccelerationX.setText("Maximal acceleration X axis: " + String.valueOf(accxMax)
                    .substring(0, 8) + " m/s^2");
            TextView maxAccelerationY = (TextView) findViewById(R.id.tvMaxAccY);
            maxAccelerationY.setText("Maximal acceleration Y axis: " + String.valueOf(accyMax)
                    .substring(0, 8) + " m/s^2");
            TextView maxAccelerationZ = (TextView) findViewById(R.id.tvMaxAccZ);
            maxAccelerationZ.setText("Maximal acceleration Z axis: " + String.valueOf(acczMax)
                    .substring(0, 8) + " m/s^2");
            TextView time = (TextView) findViewById(R.id.tvTotalTime);
            time.setText("Total time: " + String.valueOf(totalTime).substring(0, 8) + " s");
        } else {
            Toast.makeText(this, "Please choose file before looking for result", Toast.LENGTH_SHORT).show();
        }
    }

    public void returnClicked(View view) {
        finish();
    }
}