package edu.ib.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.google.api.client.util.Charsets;
import com.google.common.io.CharStreams;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ActivitySendFiles extends Activity {
    ListView listView;
    protected File[] files;
    protected String[] fileNamesArr;
    protected Uri uri;
    protected String DIRECTORY_PATH = Environment.getExternalStorageDirectory().getPath() + "/Shimmer/";
    protected static final String FILE_TYPE = "text/";
    protected ArrayAdapter<String> filesArrayAdapter;
    protected String FILE_PROVIDER_AUTHORITY = "com.shimmerresearch.shimmer.fileprovider";
    public static final String INTENT_EXTRA_DIR_PATH = "DirectoryPath";
    public static final String INTENT_EXTRA_PROVIDER_AUTHORITY = "FileProviderAuthority";
    public static String csv;
    public static String title;

    protected AdapterView.OnItemClickListener fileClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            view.setSelected(true);
            String fileExtension = StringUtils.substringAfterLast(ActivitySendFiles.this.fileNamesArr[i], ".");
            Intent intent = new Intent("android.intent.action.VIEW");
            uri = FileProvider.getUriForFile(ActivitySendFiles.this, ActivitySendFiles.this.FILE_PROVIDER_AUTHORITY, ActivitySendFiles.this.files[i]);
            title = ActivitySendFiles.this.files[i].toString();
            try (InputStream content = getContentResolver().openInputStream(uri)) {
                String result = CharStreams.toString(new InputStreamReader(
                        content, Charsets.UTF_8));
                System.out.println("fouagbaignakgbabgagb");
                StringBuilder builder = new StringBuilder();
                boolean first = true;
                double speed = 0;
                double t0 = 0;
//                builder.append("t[ms],v[m/s],a[m/s^2]\n");
                builder.append("t[ms],ax[m/s^2],ay[m/s^2],az[m/s^2]\n");
                for (String res : result.split("\n")) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    String[] parts = res.split(",");
                    if (t0 == 0) {
                        t0 = Double.parseDouble(parts[0]);
                    }
                    builder.append(Double.parseDouble(parts[0]) - t0).append(",");
                    builder.append(parts[1]).append(",").append(parts[2]).append(",").append(parts[3]).append("\n");
                }
                csv = builder.toString();
                Toast.makeText(getApplicationContext(),"FIle selected, return to main and click Show Chosen Result button or send it here", Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_files);
        setupList();
    }

    /**
     * Method that attaches file and opens dialog window with possible email oriented apps
     * @param view
     */
    public void sendviaEmail(View view) {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("vnd.android.cursor.dir/email");
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(emailIntent , "Send email..."));

    }

    /**
     * method to retrieve directory paths
     */
    private void retrieveAnyStringExtras() {
        String dirPath = this.getIntent().getStringExtra("DirectoryPath");
        String providerAuthority = this.getIntent().getStringExtra("FileProviderAuthority");
        if (dirPath != null && !dirPath.equals("")) {
            DIRECTORY_PATH = dirPath;
        }

        if (providerAuthority != null && !providerAuthority.equals("")) {
            FILE_PROVIDER_AUTHORITY = providerAuthority;
        }

    }

    /**
     * Method that creates list of files from application default directory
     */
    protected void setupList(){
        retrieveAnyStringExtras();
        setResult(0);
        File fileDir = new File(this.DIRECTORY_PATH);
        if (!fileDir.exists()) {
            Toast.makeText(this, "Error! Directory does not exist.", Toast.LENGTH_SHORT).show();
            this.finish();
        } else {
            this.files = fileDir.listFiles();
            if (this.files != null) {
                ArrayList<String> fileNamesList = new ArrayList();
                File[] var3 = this.files;
                int var4 = var3.length;
                for (int var5 = 0; var5 < var4; ++var5) {
                    File file = var3[var5];
                    fileNamesList.add(file.getName());
                }
                fileNamesArr = new String[fileNamesList.size()];
                fileNamesArr = (String[]) fileNamesList.toArray(fileNamesArr);
                filesArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_activated_1, fileNamesArr);
                listView = findViewById(R.id.list_view);
                listView.setAdapter(filesArrayAdapter);
                listView.setOnItemClickListener(fileClickListener);


            } else {
                Toast.makeText(this, "Error! Could not retrieve files from directory", Toast.LENGTH_SHORT).show();
            }

        }}



    public void returnClicked(View view) {
        finish();
    }
}